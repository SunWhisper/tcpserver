package org.academiadecodigo.bootcamp29;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class TcpClient {


    public static void main(String[] args) {

        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        String hostIP = null;
        String username = null;
        int portNumber = 55055;

        try {
            System.out.print("Server IP? ");
            hostIP = input.readLine();

            Socket clientSocket = new Socket(hostIP, portNumber);


            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
            System.out.println("What's your username?");
            username = input.readLine();
            out.println(username);

            System.out.println("Connected to server as "+username);

            while (true) {
                BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));


                System.out.println("Insert your message below:");
                String message = null;
                message = username+" "+input.readLine();
                out.println(message);

                System.out.println("Their Message:");
                System.out.println(in.readLine());

            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
