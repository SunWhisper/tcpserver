package org.academiadecodigo.bootcamp29;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class TcpServer {

    public static void main(String[] args) {

        TcpServer tcpServer = new TcpServer();

        tcpServer.start();

    }

    private void start() {

        int portNumber = 55055;
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

        try {

            ServerSocket serverSocket = new ServerSocket(portNumber);
            System.out.println("Waiting for client connection...");
            Socket clientSocket = serverSocket.accept();
            System.out.println("Client Connected.");

            BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            String username = in.readLine();
            System.out.println("user "+username+" connected");

            while (true) {

                this.listen(clientSocket, username);

                PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);




                System.out.println("Your Message:");
                String message = null;
                message = input.readLine();
                out.println(message);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void listen(Socket clientSocket, String username) throws IOException {

        BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

        System.out.println("user is writing please wait...");

        System.out.println(username +" "+ in.readLine());

    }


}
